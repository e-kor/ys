# Preparation
- [x] readme, project structure
- [x] project files
  - [x] gitignore
  - [x] makefile
  - [x] .env, docker-compose
- [x] TODO
- [ ] gitlab repo
- [ ] move python packages
  - [ ] adjust Makefile, CI (lint+test), docker-compose


# Scheduler
- [ ] structure
- [ ] dummy code, dummy tests
  - [ ] adjust Makefile, CI (lint+test)
  - [ ] adjust docker-compose (add kafka cluster, kafka-ui)
- [ ] reading postgres
- [ ] writing to kafka
  - [ ] multiple workers
- [ ] load test on local machine
# Worker
- [ ] structure
- [ ] dummy service
- [ ] reading from kafka
- [ ] service check
- [ ] writing to clickhouse
