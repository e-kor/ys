# YS

*current [TODO](docs/TODO.md)*

app for checking out status of instance at a service


Example of usage:

If we have project *taxi*, we want to make sure, that each service
(*nsk*, *ekat*) provides our favourite cars like *granta* and *kalina*.

So at first we create project *taxi*:
```shell
$ ys project create -n taxi
```
services *nsk* and *ekat*:
```shell
$ ys service create -n nsk -u nsk.taxi.ru
$ ys service create -n ekat -u ekat.taxi.ru
```
instances  *granta* and *kalina*:
```shell
$ ys instance create -n granta
$ ys instance create -n kalina
```
then we create check for instance at each service
```shell
$ ys check create -s nsk -i granta
$ ys check create -s nsk -i kalina
$ ys check create -s ekat -i granta
$ ys check create -s ekat -i kalina
```

after that, each of services will be checked for status of granta and
kalina. Every *n* minutes **ys** will make GET request for url
```nsk.taxi.ru/?instance=granta ```
and will expect answer in the following form:

```yaml
 {
  “status”: “OK”, #"OK/WARN/CRIT"
  “meta”:
    {
      "type": "arbitrary json",
      "why": "i dunno, some meta information"
    }
}
```

# Architecture

## Overview

Principal components are shown in the diagram below
![architecture](docs/ys-principal.png?raw=true "Architecture")
[editable diagram](https://lucid.app/lucidchart/6e8b233d-9353-48e0-a972-51b8abde11de/edit?invitationId=inv_2e018051-0232-4ce2-be59-946e17a072a7 )

The components to be implemented were:

- CLI
- API
- scheduler
- workers

Previously **ys** was implemented purely on python, at this point we're moving towards GO. So at first step the last two
components will be rewritten in GO:
![architecture](docs/ys-realisation.png?raw=true "Architecture")
[editable diagram](https://lucid.app/lucidchart/ca10d5a5-00de-42ee-85b2-d51f98f4d366/edit?invitationId=inv_35bb6ea8-3a78-4fa3-bfab-16f212d9817e)

## Components

### CLI

implemented in Python using Typer. Will be moved to GO at the next step.

### API

implemented in Python using Django. Pretty handy. Will be moved to GO at the last step.

### Scheduler

Scheduler is of one instance.
Reads current checks from Postgres, writes messages to Kafka.
Two main goroutines:

- periodically updates list of checks from Postgres
- periodically writes individual TaskInfo to kafka (multiple workers)

### Worker

Workers are independent of one another, stateless and horizontally-scalable.

Three main goroutines:
- reads messages from kafka, writes to TaskInfo channel
- spawns goroutines for service checks, writes results to TaskResult channel
- each second forms batch of TaskResults, writes them to Clickhouse
